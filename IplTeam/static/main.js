$('button').on('click',function(event){

  event.preventDefault();

  let element = $(this);

  $.ajax({

    url : '/like_team/',
    type : 'GET',
    data : { team_id : element.attr("data-id")},
    success :function(response){
        element.html('Like '+response)
    },
    failure : function(response){
      console.log(response)
    }
  })

});
