# Generated by Django 2.0.5 on 2018-05-18 08:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('IplTeam', '0009_team_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='Likes',
            field=models.IntegerField(default=0),
        ),
    ]
