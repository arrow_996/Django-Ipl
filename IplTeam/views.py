from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from .models import Team
from .forms import TeamForm,LoginForm
from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate

# Create your views here.
def start(request):
    teams = Team.objects.all()
    # form = TeamForm()
    return render(request,'index.html',{'teams':teams})

def detail(request,id):
    teams = Team.objects.get(id=id)
    return render(request,'detail.html',{'teams':teams})

def post_data(request):
    form = TeamForm(request.POST,request.FILES)
    if form.is_valid():
        # team = Team(team_name = form.cleaned_data['team_name'],
        #             team_url=form.cleaned_data['team_url'],
        #             team_captain=form.cleaned_data['team_captain'],
        #             team_players=form.cleaned_data['team_players'])
        # team.save()
        team = form.save(commit = False)
        team.user = request.user
        team.save()
    return HttpResponseRedirect("/")

def add_player(request):
    form = TeamForm()
    return render(request,'add_player.html',{'form':form})


def profile(request,username):
    user = User.objects.get(username=username)
    teams = Team.objects.filter(user=user)
    return render(request,'profile.html',{'username':username,
                                          'teams':teams})


# def login_view(request):
#     if request.method=="POST":
#         #get the data from the form
#         form = LoginForm(request.POST)
#         #check if the form is validation
#         if form.is_valid():
#             #get the cleaned username and password_validation
#             u = form.cleaned_data["username"]
#             p = form.cleaned_data["password"]
#             user = authenticate(username=u,password=p)
#             if user is not None:
#                 if user.is_active:
#                     #login the user
#                     login(request,user)
#                     HttpResponseRedirect("/")
#                 else:
#                     print("The account is disabled")
#             else:
#                 print("incorrect pass_word")
#     else:
#         print("else method of the POST test")
#         form=LoginForm()
#     return render(request,'login.html',{'form':form})

def login_view(request):
    if request.method == "POST":
        # get the data from the form
                form = LoginForm(request.POST)
                #check if the form is validation
                if form.is_valid():
                    #get the cleaned username and password_validation
                    u = form.cleaned_data["username"]
                    p = form.cleaned_data["password"]
                    user = authenticate(username=u,password=p)
                    print(user)
                    if user is not None:
                        print("user present")
                        if user.is_active:
                            #login the user
                            print("user is active")
                            value = login(request,user)
                            print("returned login value",value)
                            return HttpResponseRedirect("/")
                        else :
                            print("the user is not active")

                    else:
                        print("incorrect pass_word")
    else:
        form = LoginForm();
        return render(request,'login.html',{'form':form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')

def  like_team(request):
    team_id = request.GET.get("team_id",None)
    likes = 0
    if team_id :
        team = Team.objects.get(id = int(team_id))
        if team is not None:
            likes = team.likes+1
            team.likes = likes
            team.save()
    return HttpResponse(likes)
