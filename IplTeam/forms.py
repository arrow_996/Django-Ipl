from django import forms
from .models import Team
# Create your models here.
# class TeamForm(forms.Form):
class TeamForm(forms.ModelForm):
    # team_name = forms.CharField(label="name",max_length=100)
    # team_url=forms.ImageField(label="url" ,max_length=255)
    # team_captain = forms.CharField(label="captain" ,max_length=35)
    # team_players= forms.DecimalField(label="count",max_digits=10,decimal_places=0)
    class Meta:
        model = Team
        fields = ['team_name','teamImage','team_captain','team_players']

class LoginForm(forms.Form):
    username=forms.CharField(max_length=64,label="user name")
    password = forms.CharField(widget=forms.PasswordInput)
