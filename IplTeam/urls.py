from django.conf.urls import url,re_path
from django.conf import settings
from django.views.static import serve
from . import views

# ... the rest of your URLconf goes here ...
urlpatterns=[];
if settings.DEBUG:
    urlpatterns += [
        re_path(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]

#from django.conf.urls.static import static


urlpatterns += [
    url(r'^$',views.start),
    url(r'^([0-9]+)/$',views.detail,name="detail"),
    url(r'^add_player/post_url/$',views.post_data,name="post_data"),
    url(r'^add_player/$',views.add_player,name="add_player"),
    url(r'^user/(\w+)/$',views.profile,name="profile"),
    url(r'^login/$',views.login_view,name="login"),
    url(r'^logout/$',views.logout_view,name="logout"),
    url(r'^like_team/$',views.like_team,name="like")
    ]
