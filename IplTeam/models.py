from django.db import models
from django.contrib.auth.models import User


# Since Django 2.0 the ForeignKey field requires two positional arguments:
#
# the model to map to
# the on_delete argument

# Create your models here.
class Team(models.Model):
    user = models.ForeignKey(User,on_delete=models.PROTECT)
    team_name = models.CharField(max_length=100)
    teamImage=models.ImageField(upload_to='Team_logo',default="media/default.png")
    team_captain = models.CharField(max_length=35)
    team_players= models.DecimalField(max_digits=10,decimal_places=0)
    likes = models.IntegerField(default=0)
